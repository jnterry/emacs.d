;; Always enable abbreviation mode
(setq-default abbrev-mode t)

;; Save abbreviations upon exiting xemacs
(setq save-abbrevs t)

;; Reads the abbreviations file on startup
(quietly-read-abbrev-file)
