;; Delete whitespace at end of lines
(add-hook 'before-save-hook 'delete-trailing-whitespace)
